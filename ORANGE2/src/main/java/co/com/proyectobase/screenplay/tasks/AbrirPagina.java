package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.userinterface.OrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;



public class AbrirPagina implements Task {
	

private OrangePage orangePage;
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(orangePage));
		actor.attemptsTo(Click.on(OrangePage.LOGIN));
	}
	public static AbrirPagina AbrirpagOrange() {
		// TODO Auto-generated method stub
		return Tasks.instrumented(AbrirPagina.class);
	}

	
	
}
