package co.com.proyectobase.screenplay.tasks;


import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.userinterface.DetallesPage;
import co.com.proyectobase.screenplay.userinterface.PrincipalPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByValueFromBy;

public class AgregarEmpleado implements Task{

	private List<List<String>> data;
	private int identificador;
	
	
	
	public AgregarEmpleado(List<List<String>> data, int identificador) {
		super();
		this.data = data;
		this.identificador = identificador;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(PrincipalPage.PIM));
		actor.attemptsTo(Click.on(PrincipalPage.AGREGAR_EMPLEADO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		//actor.attemptsTo(Click.on(PrincipalPage.PRIMER_NOMBRE));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(1).trim()).into(PrincipalPage.PRIMER_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(2).trim()).into(PrincipalPage.SEGUNDO_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(3).trim()).into(PrincipalPage.APELLIDOS));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(4).trim()).into(PrincipalPage.ID_EMPLEADO));
		actor.attemptsTo(Click.on(PrincipalPage.UBICACION));
		
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(5).trim()+ "')]"));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.CLI));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(6).trim()).into(PrincipalPage.USER_NAME));
		actor.attemptsTo(Click.on(PrincipalPage.ESTADO));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(7).trim()+ "')]"));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(8).trim()).into(PrincipalPage.PASSWORD));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(9).trim()).into(PrincipalPage.CONFIRMAR_PASSWORD));
		actor.attemptsTo(Click.on(PrincipalPage.ESS_ROLE));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(10).trim()+ "')]"));
		actor.attemptsTo(Click.on(PrincipalPage.SUPERVISOR_ROLE));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(11).trim()+ "')]"));
		actor.attemptsTo(Click.on(PrincipalPage.ADMIN_ROLE));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(12).trim()+ "')]"));
		actor.attemptsTo(Click.on(PrincipalPage.GUARDAR));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetallesPage.OTRO_ID));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(13).trim()).into(DetallesPage.OTRO_ID));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(14).trim()).into(DetallesPage.FECHA));
		actor.attemptsTo(Click.on(DetallesPage.ESTADO_CIVIL));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(15).trim()+ "')]"));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetallesPage.GENERO));
		actor.attemptsTo(Click.on(DetallesPage.NACIONALIDAD));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(16).trim()+ "')]"));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(17).trim()).into(DetallesPage.LICENCIA));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(18).trim()).into(DetallesPage.FECHA_LICENCIA));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(19).trim()).into(DetallesPage.NICKNAME));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(20).trim()).into(DetallesPage.LIBRETA_MILITAR));
		actor.attemptsTo(Click.on(DetallesPage.GUARDAR));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.GUARDAR_3));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.REGION));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(21).trim()+ "')]"));
		actor.attemptsTo(Click.on(PrincipalPage.FTE));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(22).trim()+ "')]"));
		actor.attemptsTo(Click.on(PrincipalPage.DEPTO));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(identificador).get(23).trim()+ "')]"));
		actor.attemptsTo(Click.on(PrincipalPage.GUARDAR_4));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.GUARDAR_5));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.GUARDAR_6));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.SAVE));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
	
		actor.attemptsTo(Click.on(PrincipalPage.LISTA_EMPLEADOS));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalPage.BUSCAR_EMPLEADOS));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(4).trim()).into(PrincipalPage.BUSCAR_EMPLEADOS));
	}

	public static Performable Agregar(List<List<String>> data,int id) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(AgregarEmpleado.class,data,id);
	}

}
