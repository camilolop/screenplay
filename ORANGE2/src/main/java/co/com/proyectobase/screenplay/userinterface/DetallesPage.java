package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class DetallesPage extends PageObject{

	public static final Target OTRO_ID= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'otherId\']"));
	
	public static final Target FECHA= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'dateOfBirth\']"));
	
	public static final Target ESTADO_CIVIL= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'maritalStatus_inputfileddiv\']/div/input"));
	
	
	public static final Target GENERO= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'pimPersonalDetailsForm_PimPersonalDetail\']/materializecss-decorator[3]/div/sf-decorator[1]/div/ul/li[2]"));
	
	
	public static final Target NACIONALIDAD= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\"nationality_inputfileddiv\"]/div/input"));
	
	public static final Target LICENCIA= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'txtLicenNo\']"));
	
	public static final Target FECHA_LICENCIA= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'txtLicExpDate\']"));
	
	
	public static final Target NICKNAME= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'nickName\']"));
	
	
	public static final Target LIBRETA_MILITAR= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\"txtLicenNo\"]"));
	
	public static final Target FUMADOR= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'pimPersonalDetailsForm_PimPersonalDetail\']/materializecss-decorator[5]/div/sf-decorator[2]/div/label"));
	
	public static final Target GUARDAR= Target.the("Registrar ID diferente")
			.located(By.xpath("//*[@id=\'content\']/div[2]/ui-view/div[2]/div/div[3]/button[2]"));
	
	
}
