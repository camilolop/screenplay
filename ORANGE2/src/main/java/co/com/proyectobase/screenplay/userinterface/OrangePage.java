package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.By;
@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class OrangePage extends PageObject{

	public static final Target LOGIN= Target.the("Ingresa Aplicacion con usuario y contraseña")
			.located(By.xpath("//*[@id=\'btnLogin\']"));
	
}
