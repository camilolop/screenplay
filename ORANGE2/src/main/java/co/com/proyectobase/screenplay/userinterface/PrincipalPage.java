package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class PrincipalPage extends PageObject{
	
	public static final Target PIM= Target.the("Modulo de Información personal")
			.located(By.xpath("//*[@id=\'menu_pim_viewPimModule\']/a/span[2]"));
	
	public static final Target AGREGAR_EMPLEADO= Target.the("Agregar Empleado")
			.located(By.xpath("//*[@id=\'menu_pim_addEmployee\']/span[2]"));
	
	public static final Target PRIMER_NOMBRE= Target.the("Primer Nombre")
			.located(By.xpath("//*[@id=\'firstName\']"));

	public static final Target SEGUNDO_NOMBRE= Target.the("Segundo Nombre")
			.located(By.xpath("//*[@id=\'middleName\']"));
	
	public static final Target APELLIDOS= Target.the("Apellidos")
			.located(By.xpath("//*[@id=\'lastName\']"));
	
	public static final Target ID_EMPLEADO= Target.the("Id Empleado")
			.located(By.xpath("//*[@id=\'employeeId\']"));
	
	public static final Target UBICACION= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'location_inputfileddiv\']/div/input"));
	
	public static final Target MODAL= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'location\']"));
	
	public static final Target CLI= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\"pimAddEmployeeForm\"]/div[1]/div/materializecss-decorator[3]/div/sf-decorator/div/label"));
	
	public static final Target USER_NAME= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'username\']"));
	
	public static final Target ESTADO= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'status_inputfileddiv\']/div/input"));
	
	public static final Target PASSWORD= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'password\']"));
	
	public static final Target CONFIRMAR_PASSWORD= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'confirmPassword\']"));
	
	public static final Target ESS_ROLE= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'essRoleId_inputfileddiv\']/div/input"));
	
	public static final Target SUPERVISOR_ROLE= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'supervisorRoleId_inputfileddiv\']/div/input"));
	
	public static final Target ADMIN_ROLE= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'adminRoleId_inputfileddiv\']/div/input"));
	
	public static final Target GUARDAR= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\"systemUserSaveBtn\"]"));
	
	public static final Target GUARDAR_3= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'content\']/div[2]/ui-view/div[2]/div/div[3]/button[2]"));
	
	public static final Target REGION= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'WizardFieldDefinition9_inputfileddiv\']/div/input"));
	
	public static final Target FTE= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'WizardFieldDefinition10_inputfileddiv\']/div/input"));
	
	public static final Target DEPTO= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\"WizardFieldDefinition11_inputfileddiv\"]/div/input"));
	
	public static final Target GUARDAR_4= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'content\']/div[2]/ui-view/div[2]/div/div[3]/button[2]"));
	
	public static final Target GUARDAR_5= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'content\']/div[2]/ui-view/div[2]/div/div[3]/button[2]"));
	
	public static final Target GUARDAR_6= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'content\']/div[2]/ui-view/div[2]/div/div[3]/button[2]"));
	
	public static final Target SAVE= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/div[3]/button[3]"));
	
	public static final Target LISTA_EMPLEADOS= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'menu_pim_viewEmployeeList\']/span[2]"));
	
	
	public static final Target BUSCAR_EMPLEADOS= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'employee_name_quick_filter_employee_list_value\']"));
	
	public static final Target TABLA_EMPLEADOS= Target.the("Ubicacion Empleado")
			.located(By.xpath("//*[@id=\'employeeListTable\']/tbody/tr/td[2]"));
	
	
	
	
}
