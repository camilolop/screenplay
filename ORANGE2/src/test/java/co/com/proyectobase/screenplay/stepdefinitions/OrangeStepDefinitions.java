package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.AbrirPagina;
import co.com.proyectobase.screenplay.tasks.AgregarEmpleado;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class OrangeStepDefinitions {
	
	@Before
	public void configuracionInicial() {
		juan.can(BrowseTheWeb.with(hisBrowser));
	}

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("juan");
	
	@Given("^que Juan necesita crear un empleado en el OrageHR$")
	public void queJuanNecesitaCrearUnEmpleadoEnElOrageHR() throws Exception {
		 juan.wasAbleTo(AbrirPagina.AbrirpagOrange());
	  
	}


	@When("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaAplicación(DataTable dtDataForm) throws Exception {
		List<List<String>> data = dtDataForm.raw();
		
		for(int i=0; i <data.size();i++) {
		juan.attemptsTo(AgregarEmpleado.Agregar(data,i));
		
		
	
		
		juan.attemptsTo(Esperar.aMoment());
		}
	}

	@Then("^el visualiza el nuevo empleado en el aplicativo$")
	public void elVisualizaElNuevoEmpleadoEnElAplicativo() throws Exception {
	   
		juan.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo("8526")));
		
	    
	}
}
