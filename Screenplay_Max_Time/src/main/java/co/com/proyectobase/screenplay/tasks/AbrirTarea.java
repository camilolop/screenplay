package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.ReporteDetallado;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class AbrirTarea implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(ReporteDetallado.NUEVO));
		
	}

	public static AbrirTarea AbroForm() {
		// TODO Auto-generated method stub
		return Tasks.instrumented(AbrirTarea.class);
	}

}
