package co.com.proyectobase.screenplay.tasks;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.userinterface.ReporteDetallado;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.findby.By;

public class CerrarDia implements Task{
private WebDriver driver;
	public CerrarDia(WebDriver driver) {
	super();
	this.driver = driver;
}



	@Override
	public <T extends Actor> void performAs(T actor) {

	
	}
	
	
	
	public static CerrarDia cierroDia(WebDriver driver) {
		
		return Tasks.instrumented(CerrarDia.class,driver);
	}

	
	
}
