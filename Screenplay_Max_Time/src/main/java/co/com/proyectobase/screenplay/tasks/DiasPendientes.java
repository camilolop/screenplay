package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.userinterface.PrincipalMaxTimePage;
import net.serenitybdd.screenplay.Actor;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class DiasPendientes implements Task{
	private List<List<String>> data;
	private int identificador;
	
	
	public DiasPendientes(List<List<String>> data, int identificador) {
		this.data = data;
		this.identificador = identificador;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalMaxTimePage.FILTRO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(SeleccionarLista.Desde(PrincipalMaxTimePage.FILTRO2, data.get(identificador).get(2).trim()));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(PrincipalMaxTimePage.ENTRAR_DIA));
		actor.attemptsTo(Esperar.aMoment());
		
	}

	public static DiasPendientes Ingreso_da(List<List<String>> datos, int identificador) {
		return Tasks.instrumented(DiasPendientes.class,datos,identificador);
	}

	
}
