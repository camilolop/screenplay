package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.userinterface.DetalleReporteDia;
import co.com.proyectobase.screenplay.userinterface.MaxTimePage;
import co.com.proyectobase.screenplay.userinterface.ReporteDetallado;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.findby.By;

public class IngresarDetalleReporteDia implements Task {
	private List<List<String>> data;
	private int identificador;
	private  WebDriver driver,driver1;
	
	public IngresarDetalleReporteDia(List<List<String>> data, int identificador, WebDriver driver,WebDriver driver1) {
		this.data = data;
		this.identificador = identificador;
		this.driver = driver;
		this.driver1=driver1;
	}
	
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.BTN_PROYECTO));
		actor.attemptsTo(Esperar.aMoment());
		driver.switchTo().frame(0);
		actor.attemptsTo(Click.on(DetalleReporteDia.BTN_PROYECTO_CLICK));
		driver.switchTo().defaultContent();
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.BTN_HORA_PROYECTO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(SeleccionarLista.Desde(DetalleReporteDia.LISTA_HORAS, data.get(identificador).get(4).trim()));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.SERVICIO));
		actor.attemptsTo(Esperar.aMoment());
		driver.switchTo().frame(0);
		actor.attemptsTo(Click.on(DetalleReporteDia.TXT_SERVICIO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(5).trim()).into(DetalleReporteDia.TXT_SERVICIO));
		actor.attemptsTo(Click.on(DetalleReporteDia.BTN_SEARCH));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.SELECCIONAR_PROY));
		driver.switchTo().defaultContent();
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.BTN_ACTIVIDAD));
		actor.attemptsTo(SeleccionarLista.Desde(DetalleReporteDia.LISTA_ACTIVIDAD, data.get(identificador).get(6).trim()));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.BTN_ACTIVIDAD));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(7).trim()).into(DetalleReporteDia.TXT_HORAS));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(DetalleReporteDia.TXT_COMENTARIO));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(8).trim()).into(DetalleReporteDia.TXT_COMENTARIO));
		actor.attemptsTo(Click.on(DetalleReporteDia.GUARDAR_CERRAR));
		actor.attemptsTo(Esperar.aMoment());
		
		
		
				
		
	
		
	
	
		
        
   
	
	
	}

	public static IngresarDetalleReporteDia Proyecto(List<List<String>> data, int id, WebDriver driver,WebDriver driver1) {
		//
		return Tasks.instrumented(IngresarDetalleReporteDia.class,data,id,driver,driver1 );
	}

}
