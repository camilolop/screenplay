package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.interactions.SeleccionarLista;

import co.com.proyectobase.screenplay.userinterface.MaxTimePage;
import co.com.proyectobase.screenplay.userinterface.PrincipalMaxTimePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class IngresarMaxTime implements Task {
	private  List<List<String>> data;
	private  int identificador;

	public IngresarMaxTime(List<List<String>> data, int identificador) {
		
		this.data = data;
		this.identificador = identificador;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(MaxTimePage.USUARIO));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(1).trim()).into(MaxTimePage.USUARIO));
		actor.attemptsTo(Click.on(MaxTimePage.CONTRASEÑA));
		actor.attemptsTo(Enter.theValue(data.get(identificador).get(2).trim()).into(MaxTimePage.CONTRASEÑA));
		actor.attemptsTo(Click.on(MaxTimePage.INGRESO));
		actor.attemptsTo(Esperar.aMoment());
		
	
	}

	public static IngresarMaxTime Ingreso(List<List<String>> datos, int id) {
		
		return Tasks.instrumented(IngresarMaxTime.class,datos,id);
	}

}
