package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.stepdefinitions.MaxTimeStepDefinitions;
import co.com.proyectobase.screenplay.userinterface.MaxTimePage;
import co.com.proyectobase.screenplay.userinterface.ReporteDetallado;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Text;
import net.thucydides.core.annotations.findby.By;

public class IngresarReporteDetallado implements Task {
	private List<List<String>> data;
	private int identificador;
	private String dia_lab;
	private WebDriver driver;

	public IngresarReporteDetallado(List<List<String>> data, int identificador, WebDriver driver) {

		this.data = data;
		this.identificador = identificador;
		this.driver = driver;

	}

	@Override
	public <T extends Actor> void performAs(T actor) {

		/*if (((driver.findElement(By.xpath(
				"//table[contains(@id,'_dviLaboral_View')]/tbody/tr/td//span/input[contains(@id,'xaf_dviLaboral_View_S')]"))
				.getAttribute("value").equals("U"))
				|| (driver
						.findElement(By.xpath(
								"//*[@id=\'Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View\']"))
						.getText().equals("9"))) )  {
			
				actor.attemptsTo(Click.on(ReporteDetallado.CERRAR_DIA));
		
				
		
			

		} else {
			if ((driver.findElement(By.xpath(
					"//table[contains(@id,'_dviLaboral_View')]/tbody/tr/td//span/input[contains(@id,'xaf_dviLaboral_View_S')]"))
					.getAttribute("value").equals("U"))
					|| (driver
							.findElement(By.xpath(
									"//*[@id=\'Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View\']"))
							.getText().equals("9"))) {

		}
	else {

				actor.attemptsTo(Click.on(ReporteDetallado.NUEVO));
			}
		}*/
		actor.attemptsTo(Click.on(ReporteDetallado.CERRAR_DIA));
	}

	public static IngresarReporteDetallado Ingreso_Reporte(List<List<String>> data, int id, WebDriver driver) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(IngresarReporteDetallado.class, data, id, driver);
	}

}
