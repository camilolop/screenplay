package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class DetalleReporteDia extends PageObject{
	
	
	public static final Target BTN_PROYECTO= Target.the("Boton de ingresar")
			.located(By.xpath("//*[@id=\"Vertical_v6_MainLayoutEdit_xaf_l128_xaf_dviProyecto_Edit_Find_BImg\"]"));

	public static final Target BTN_PROYECTO_CLICK= Target.the("Boton de ingresar")
			.located(By.xpath("//table[contains(@class,'dxgvTable_Office2010Blue')]/tbody/tr[2]/td[4]/table/tbody/tr/td/span"));


	public static final Target BTN_HORA_PROYECTO= Target.the("Boton hora proyecto")
			.located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l148_xaf_dviTipoHora_Edit_DD_B-1Img\']"));
	
	public static final Target LISTA_HORAS= Target.the("Listas de tipos de horas de proyecto")
			.located(By.xpath("//*[@id=\"Vertical_v6_MainLayoutEdit_xaf_l148_xaf_dviTipoHora_Edit_DD_DDD_L_LBT\"]/tbody"));
	
	public static final Target SERVICIO= Target.the("Tipo de Servicio")
			.located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l153_xaf_dviServicio_Edit_Find_BImg\']"));
	
	
	public static final Target TXT_SERVICIO= Target.the("Escribir Tipo de Servicio")
			.located(By.xpath("//*[@id=\'Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_Ed_I\']"));
	
	public static final Target BTN_SEARCH= Target.the("boton para buscar servicio")
			.located(By.xpath("//*[@id=\'Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_CD\']/span"));
	
	public static final Target SELECCIONAR_PROY= Target.the("Boton para aceptar servicio")
			.located(By.xpath("	//table[contains(@id,'Dialog_Table1')]/tbody/tr[2]/td/div/div/div/table//tbody/tr[2]/td[3]\r\n" + 
					""));
	
	public static final Target BTN_ACTIVIDAD= Target.the("Boton de actividad")
			.located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l158_xaf_dviActividad_Edit_DD_B-1Img\']"));
	
	public static final Target LISTA_ACTIVIDAD= Target.the("Lista de tip de actividad")
			.located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l158_xaf_dviActividad_Edit_DD_DDD_L\']/tbody/tr"));
	
	public static final Target TXT_HORAS= Target.the("Digitar horas")
			.located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l182_xaf_dviHoras_Edit_I\']"));
	
	public static final Target TXT_COMENTARIO= Target.the("Digitar Comentario")
			.located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l207_xaf_dviComentario_Edit_I\']"));
	
	public static final Target GUARDAR_CERRAR= Target.the("Guardar")
			.located(By.xpath("//*[@id=\'Vertical_EditModeActions2_Menu_DXI1_Img\']"));
	
	
	
	
	
	
	
	
	
}
