package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.By;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxTimeCHC/Login.aspx")
public class MaxTimePage extends PageObject {

	public static final Target USUARIO= Target.the("Ingresa el Usuario")
			.located(By.xpath("//INPUT[@id='Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I']"));


	public static final Target CONTRASEÑA= Target.the("Ingresa la Contraseña")
			.located(By.xpath("//INPUT[@id='Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I']"));

	public static final Target INGRESO= Target.the("Boton de ingresar")
			.located(By.xpath("//A[@title='Conectarse'][text()='Conectarse']"));


}
