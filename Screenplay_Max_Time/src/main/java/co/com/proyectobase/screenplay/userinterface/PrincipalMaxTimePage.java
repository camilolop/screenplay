package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class PrincipalMaxTimePage extends PageObject {
	
	public static final Target ENTRAR_DIA= Target.the("seleccionar dia").located(By.xpath("//table[contains(@class,'dxgvTable_Office2010Blue')]/tbody/tr[2]/td[3]/table/tbody/tr/td[contains(@class,'WebEditorCell')]"));
	public static final Target FILTRO= Target.the("Analista").located(By.xpath("//table[contains(@class,'dxgvTable_Office2010Blue')]/tbody/tr[1]/td[4]/table/tbody/tr/td[2]/img"));
	public static final Target FILTRO2= Target.the("Lista de Analistas").located(By.xpath("//*[@id=\"Vertical_v1_LE_v2_DXHFP_PWC-1\"]/table/tbody/tr/td"));
	
}
