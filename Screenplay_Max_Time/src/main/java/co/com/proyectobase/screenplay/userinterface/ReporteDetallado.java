package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class ReporteDetallado extends PageObject{
	
	public static final Target DIA_LABORAL= Target.the("Ingresa el Usuario")
			.located(By.xpath("//*[@id=\"Vertical_v3_MainLayoutView_xaf_l49_xaf_l49_ctbl\"]/tbody/tr/td[1]"));

	public static final Target CERRAR_DIA= Target.the("Cerra día")
			.located(By.xpath("//*[@id=\"Vertical_TB_Menu_DXI1_T\"]/a"));
	
	public static final Target NUEVO= Target.the("Ingresar Nuevo reporte")
			.located(By.xpath("//*[@id=\"Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_ToolBar_Menu_DXI0_T\"]/a"));
	
	public static final Target CONFIRMAR_CERRAR_DIA= Target.the("Confirmar cierre de dia")
			.located(By.xpath("//*[@id=\"Vertical_TB_Menu_DXI1_T\"]/a"));

}
