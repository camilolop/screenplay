package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.AbrirTarea;
import co.com.proyectobase.screenplay.tasks.CerrarDia;
import co.com.proyectobase.screenplay.tasks.DiasPendientes;
import co.com.proyectobase.screenplay.tasks.IngresarDetalleReporteDia;
import co.com.proyectobase.screenplay.tasks.IngresarMaxTime;
import co.com.proyectobase.screenplay.tasks.IngresarReporteDetallado;
import co.com.proyectobase.screenplay.userinterface.ReporteDetallado;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.questions.Text;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.findby.By;

public class MaxTimeStepDefinitions {

	@Before
	public void configuracionInicial() {
		camilo.can(BrowseTheWeb.with(hisBrowser));
	}

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor camilo = Actor.named("camilo");

	@Given("^Camilo Analista de Pruebas Quiere Ingresar a Maxtime$")
	public void camiloAnalistaDePruebasQuiereIngresarAMaxtime() throws Exception {

		camilo.wasAbleTo(Abrir.laPaginaMaxTime());

	}

	@When("^Ingresar al reporte de tiempos$")
	public void ingresarAlReporteDeTiempos(DataTable dtDatosForm) throws Exception {
		List<List<String>> data = dtDatosForm.raw();
		WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();
		WebDriver driver1 = Serenity.getWebdriverManager().getCurrentDriver();

		camilo.attemptsTo(IngresarMaxTime.Ingreso(data, 0));
		camilo.attemptsTo(DiasPendientes.Ingreso_da(data, 0));
		// camilo.attemptsTo(IngresarReporteDetallado.Ingreso_Reporte(data, 0, driver));
		
		for (int i = 0; i < data.size(); i++) {
			//camilo.attemptsTo(IngresarReporteDetallado.Ingreso_Reporte(data, i, driver));
			
			
			
			
			if (((driver.findElement(By.xpath(
					"//table[contains(@id,'_dviLaboral_View')]/tbody/tr/td//span/input[contains(@id,'xaf_dviLaboral_View_S')]"))
					.getAttribute("value").equals("U"))
					|| (driver
							.findElement(By.xpath(
									"//*[@id=\'Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View\']"))
							.getText().equals("9"))) )  {
				
				
				camilo.attemptsTo(IngresarReporteDetallado.Ingreso_Reporte(data, i, driver));
				break;
				
			}else {
				camilo.attemptsTo(AbrirTarea.AbroForm());
			}
			
			camilo.attemptsTo(IngresarDetalleReporteDia.Proyecto(data, i, driver, driver1));
			
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}

		}

	}

	@Then("^Cerrar Dia$")
	public void cerrarDia() throws Exception {
		

	}

}
